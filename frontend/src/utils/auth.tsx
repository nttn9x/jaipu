import { JWT_THE_KEY_OF_THE_LIFE } from "../constants/common";

export function setToken(token: string) {
  // localStorage.setItem(JWT_THE_KEY_OF_THE_LIFE, JSON.stringify(data));
  localStorage.setItem(JWT_THE_KEY_OF_THE_LIFE, token);
}

export function removeToken() {
  localStorage.removeItem(JWT_THE_KEY_OF_THE_LIFE);
}

export function getToken() {
  return localStorage.getItem(JWT_THE_KEY_OF_THE_LIFE);
}

export function getUser(): any {
  let user = null;
  try {
    const token = getToken();
    if (token) {
      user = JSON.parse(atob(token.split(".")[1]));
      user.firstString = user.username.charAt(0);
    }
  } catch (error) {
    console.log(error);
  }

  return user;
}
