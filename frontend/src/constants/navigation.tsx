import React from "react";

import HomeIcon from "../components/ui-libraries/icons/home";
import DashboardIcon from "../components/ui-libraries/icons/dashboard";
import TextFieldsIcon from "../components/ui-libraries/icons/text-fields";
import GroupIcon from "../components/ui-libraries/icons/group";
import StorageIcon from "../components/ui-libraries/icons/storage";
import CloudDownloadIcon from "../components/ui-libraries/icons/cloud-download";
import CloudUploadIcon from "../components/ui-libraries/icons/cloud-upload";
import DvrIcon from "../components/ui-libraries/icons/dvr";
import PlaylistAddCheckIcon from "../components/ui-libraries/icons/playlist-add-check";
import VerifiedUserIcon from "../components/ui-libraries/icons/verified-user";
import LibraryBooksIcon from "../components/ui-libraries/icons/library-book";

export enum ROUTES {
  Home = "/",
  Batches = "/batches",
  Dashboard = "/dashboard",
  DatabaseViewer = "/database-viewer",
  Export = "/export",
  Fields = "/fields",
  Import = "/import",
  Monitor = "/monitor",
  Quality = "/quality",
  Roles = "/Roles",
  Users = "/users"
}

export const SIDE_BAR = [
  {
    keyi18n: "home",
    linkTo: ROUTES.Home,
    icon: <DashboardIcon />
  },
  {
    keyi18n: "dashboard",
    linkTo: ROUTES.Dashboard,
    icon: <HomeIcon />
  },
  {
    keyi18n: "batches",
    linkTo: ROUTES.Batches,
    icon: <LibraryBooksIcon />
  },
  {
    keyi18n: "database_viewer",
    linkTo: ROUTES.DatabaseViewer,
    icon: <StorageIcon />
  },
  {
    keyi18n: "export",
    linkTo: ROUTES.Export,
    icon: <CloudDownloadIcon />
  },
  {
    keyi18n: "fields",
    linkTo: ROUTES.Fields,
    icon: <TextFieldsIcon />
  },
  {
    keyi18n: "export",
    linkTo: ROUTES.Import,
    icon: <CloudUploadIcon />
  },
  {
    keyi18n: "monitor",
    linkTo: ROUTES.Monitor,
    icon: <DvrIcon />
  },
  {
    keyi18n: "quality_control",
    linkTo: ROUTES.Quality,
    icon: <PlaylistAddCheckIcon />
  },
  {
    keyi18n: "roles",
    linkTo: ROUTES.Roles,
    icon: <VerifiedUserIcon />
  },
  {
    keyi18n: "users",
    linkTo: ROUTES.Users,
    icon: <GroupIcon />
  }
];
