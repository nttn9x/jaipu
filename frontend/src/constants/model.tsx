export interface User {
  _id: string;
  username: string;
  password: string;
  fullName: string;
  avatar: string;
  email: string;
  roleIds: any;
  roles?: any;
}

export interface Role {
  _id: string;
  name: string;
  url: string;
}
