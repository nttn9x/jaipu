import { useAuthDataContext } from "../../../../context/auth.context";
import useMessage from "../../../../context/message.contex";
import { User } from "../../../../constants/model";
import { HTTP_CODE } from "../../../../constants/common";

export function useOwnRedux(history: any) {
  const { isAuth, onLogin } = useAuthDataContext();
  const { pushMsgError } = useMessage();

  async function handleLogin(user: User) {
    await fetch("/auth/login", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "post",
      body: JSON.stringify(user)
    })
      .then(function(response) {
        if (response.status === HTTP_CODE.Unauthorized) {
          throw Error("Login failed!");
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(function(data) {
        onLogin(data);
        history.push("/");
      })
      .catch(function(error) {
        pushMsgError(error.message);
      });
  }

  return {
    isAuth,
    handleLogin
  };
}
