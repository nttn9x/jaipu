import { useEffect, useCallback, useReducer } from "react";

import { User } from "../../../../constants/model";
import useUserService from "./user-list.service";

import { ROUTES } from "../../../../constants/navigation";

import produce from "immer";

const ADD_DATA = "ADD_DATA";
const UPDATE_DATA = "UPDATE_DATA";
const SELECT_DATA = "SELECT_DATA";
const REMOVE_DATA = "REMOVE_DATA";
const SET_DATAS = "SET_DATAS";
const LOADING_DATAS = "LOADING_DATAS";

export interface State {
  isLoading: boolean;
  isFirstLoad: boolean;
  indexSelected: number;
  datas: User[];
}

const initialState: State = {
  isLoading: false,
  isFirstLoad: true,
  indexSelected: -1,
  datas: []
};

interface LOADING_DATAS {
  type: typeof LOADING_DATAS;
}

interface ADD_DATA {
  type: typeof ADD_DATA;
}

interface UPDATE_DATA {
  type: typeof UPDATE_DATA;
  data: User;
}

interface SELECT_DATA {
  type: typeof SELECT_DATA;
  index: number;
}

interface REMOVE_DATA {
  type: typeof REMOVE_DATA;
}

interface SET_DATAS {
  type: typeof SET_DATAS;
  datas: User[];
}

type Actions =
  | UPDATE_DATA
  | ADD_DATA
  | LOADING_DATAS
  | SELECT_DATA
  | REMOVE_DATA
  | SET_DATAS;

const reducer = produce((draft, action: Actions) => {
  switch (action.type) {
    case LOADING_DATAS:
      draft.isLoading = true;
      break;
    case ADD_DATA:
      draft.datas.unshift({ fullName: "", email: "", avatar: "", _id: "0" });
      draft.indexSelected = 0;
      break;
    case UPDATE_DATA:
      draft.datas[draft.indexSelected] = action.data;
      break;
    case SELECT_DATA:
      draft.indexSelected = action.index;
      break;
    case REMOVE_DATA:
      draft.datas.splice(draft.indexSelected, 1);
      draft.indexSelected = -1;
      break;
    case SET_DATAS:
      draft.datas = action.datas;
      draft.isFirstLoad = false;
      draft.isLoading = false;
      break;
  }
});

function useUserHook(history: any) {
  const { apiGetAllUser } = useUserService();
  const [state, dispatch] = useReducer(reducer, initialState);

  const loadData = useCallback(
    async (textsearch?: string) => {
      const result = await apiGetAllUser({ search: textsearch });

      dispatch({ type: SET_DATAS, datas: !result.isError ? result.data : [] });
    },
    [apiGetAllUser]
  );

  useEffect(() => {
    let runFirstLoad = async () => {
      await loadData();
    };

    runFirstLoad();

    const paths = history.location.pathname.split("/");
    if (paths.length > 2) {
      history.push(ROUTES.Users);
    }
  }, [loadData, history]);

  async function handleSearch(textsearch: string) {
    dispatch({ type: LOADING_DATAS });

    await loadData(textsearch);
  }

  function handleClick(index: number) {
    dispatch({ type: SELECT_DATA, index });

    history.push(`${ROUTES.Users}/${state.datas[index]._id}/${index}`);
  }

  function doCreateData() {
    if (state.datas.length > 0 && state.datas[0]._id === "0") {
      return;
    }

    dispatch({ type: ADD_DATA });

    history.push(`${ROUTES.Users}/0/0`);
  }

  function removeItemInList() {
    dispatch({ type: REMOVE_DATA });
  }

  function updateItemInList(data: User) {
    dispatch({ type: UPDATE_DATA, data });
  }

  return {
    state,
    handleSearch,
    handleClick,
    doCreateData,
    removeItemInList,
    updateItemInList
  };
}

export default useUserHook;
