import React from "react";
import styles from "./user-list.module.scss";

import List from "../../../../components/ui-own/list-rows/list-rows.component";
import Typography from "../../../../components/ui-libraries/typography";

import { State } from "../user-list/user-list.hook";

interface IUserListProps {
  state: State;
  handleSearch: any;
  handleClick: any;
  doCreateData: any;
}

const UserList: React.FC<IUserListProps> = ({
  state,
  handleSearch,
  handleClick,
  doCreateData
}) => {
  function _rowRenderer(index: number) {
    const row = state.datas[index];

    return (
      <div className={styles.list}>
        <div className={styles.list__avatar}>
          {row._id !== "0" && row.avatar ? (
            <img src={row.avatar} alt="error" />
          ) : (
            <div className={styles.list__avatar_empty} />
          )}
        </div>
        <div className={styles.list__info}>
          <Typography variant="subtitle2">{row.username}</Typography>
          <Typography className={styles.list__info_email} variant="body2">
            {row.email}
          </Typography>
        </div>
      </div>
    );
  }

  return (
    <List
      idElement="m-user-l"
      rowHeight={102}
      scrollToIndex={state.indexSelected}
      doCreateData={doCreateData}
      onRowClick={handleClick}
      handleSearch={handleSearch}
      isFirstLoad={state.isFirstLoad}
      isLoading={state.isLoading}
      indexSelected={state.indexSelected}
      rowCount={state.datas.length}
      rowRenderer={_rowRenderer}
    />
  );
};

export default UserList;
