import { useEffect, useCallback } from "react";
import axios from "axios";

import { getAllUser } from "../../../../services/user.service";

import useMessage from "../../../../context/message.contex";

const CancelToken = axios.CancelToken;
let cancel: any;

function useUserServices() {
  const { pushMsgError } = useMessage();

  function clearRequest() {
    cancel && cancel();
  }

  useEffect(() => {
    return clearRequest;
  }, []);

  const apiGetAllUser = useCallback(function apiGetAllUser(params: any): any {
    clearRequest();

    return getAllUser({
      options: {
        params,
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      },
      pushMsgError
    });
  }, [pushMsgError]);

  return { apiGetAllUser };
}

export { useUserServices as default };
