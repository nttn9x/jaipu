import React from "react";

import { Route } from "react-router-dom";

import {
  Layout,
  ColumnDetail,
  ColumnList
} from "../../../components/ui-own/layout-page/layout.component";
import UserList from "./user-list/user-list.container";
import UserItem from "./user-item/user-item.container";

import { ROUTES } from "../../../constants/navigation";

import useUserHook from "./user-list/user-list.hook";

interface IUserContainerProps {
  history: any;
}

const UserContainer: React.FC<IUserContainerProps> = ({ history }) => {
  const { removeItemInList, updateItemInList, ...stateParent } = useUserHook(
    history
  );

  return (
    <Layout>
      <ColumnList>
        <UserList {...stateParent} />
      </ColumnList>
      <ColumnDetail>
        <Route
          render={(props: any) => (
            <UserItem
              {...props}
              removeItemInList={removeItemInList}
              updateItemInList={updateItemInList}
            />
          )}
          path={`${ROUTES.Users}/:id/:index`}
        />
      </ColumnDetail>
    </Layout>
  );
};

export default UserContainer;
