import React from "react";
import styles from "./user-item.module.scss";

import { Formik } from "formik";
import UserItemSettingsComponent from "../../../../components/ui-own/actions-detail/actions-detail.component";
import UserItemFormComponent from "./user-item-form.components";
import SkeletonDetailComponent from "../../../../components/ui-own/skeleton-detail/skeleton-detail.component";

import UserSchema from "./user-item.validation";

import { useTranslation } from "react-i18next";

import useUserHook from "./user-item.hook";

interface IUserItemProps {
  history: any;
  match: any;
  handleInputChange: any;
  removeItemInList: any;
  updateItemInList: any;
}

const UserItemContainer: React.FC<IUserItemProps> = ({
  history,
  match,
  removeItemInList,
  updateItemInList
}) => {
  const { t } = useTranslation(["common"]);
  const { isLoading, blockUI, roles, data, doSave, doDelete } = useUserHook(
    history,
    match,
    updateItemInList,
    removeItemInList
  );

  if (isLoading) {
    return <SkeletonDetailComponent />;
  }

  return (
    <Formik
      initialValues={data}
      validationSchema={UserSchema}
      onSubmit={(values: any, actions) => {
        doSave(values);
      }}
    >
      {({ handleSubmit, ...rest }) => (
        <form onSubmit={handleSubmit}>
          <UserItemSettingsComponent
            isNew={data._id === "0"}
            useFormik={true}
            blockUI={blockUI}
            doSave={doSave}
            doDelete={doDelete}
          />
          <UserItemFormComponent
            roles={roles}
            t={t}
            styles={styles}
            {...rest}
          />
        </form>
      )}
    </Formik>
  );
};

export default UserItemContainer;
