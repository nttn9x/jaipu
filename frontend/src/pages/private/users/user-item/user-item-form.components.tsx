import React from "react";

import Grid from "../../../../components/ui-libraries/grid";
import TextField from "../../../../components/ui-libraries/text-field";
import InputLabel from "../../../../components/ui-libraries/input-label";
import FormControl from "../../../../components/ui-libraries/form-control";
import FormHelperText from "../../../../components/ui-libraries/form-helper-text";
import Typography from "../../../../components/ui-libraries/typography";
import Select from "react-select";
import Input from "../../../../components/ui-libraries/input";

interface IUserItemFormProps {
  t: any;
  styles: any;
  values: any;
  touched: any;
  errors: any;
  roles: any;
  handleBlur: any;
  handleChange: any;
  setFieldValue: any;
}

const UserItemFormContainer: React.FC<IUserItemFormProps> = ({
  t,
  roles,
  styles,
  values,
  touched,
  errors,
  handleBlur,
  handleChange,
  setFieldValue
}) => {
  const isErrorUserName = touched.username && Boolean(errors.username);
  const isErrorPassword = touched.password && Boolean(errors.password);

  function onChange(props: any) {
    setFieldValue("roles", props);
  }

  return (
    <Grid className={styles.form} container spacing={3}>
      <Grid item xs={6}>
        <FormControl fullWidth error={isErrorUserName}>
          <InputLabel required htmlFor="component-helper">
            {t("username")}
          </InputLabel>
          <Input
            autoComplete="new-password"
            value={values.username}
            name="username"
            inputProps={{
              "aria-label": "username"
            }}
            onBlur={handleBlur}
            onChange={handleChange}
          />
          <FormHelperText>
            {isErrorUserName && t(`${errors.username}`)}
          </FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xs={6}>
        <FormControl fullWidth error={isErrorPassword}>
          <InputLabel required htmlFor="component-helper">
            {t("password")}
          </InputLabel>
          <Input
            autoComplete="new-password"
            value={values.password}
            name="password"
            inputProps={{
              "aria-label": "password"
            }}
            onBlur={handleBlur}
            onChange={handleChange}
          />
          <FormHelperText>
            {isErrorPassword && t(`${errors.password}`)}
          </FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xs={6}>
        <TextField
          onBlur={handleBlur}
          onChange={handleChange}
          name="fullName"
          fullWidth
          label={t("fullname")}
          value={values.fullName}
          inputProps={{
            "aria-label": "fullName"
          }}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          onBlur={handleBlur}
          onChange={handleChange}
          name="email"
          fullWidth
          label={t("email")}
          value={values.email}
          inputProps={{
            "aria-label": "email"
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography variant="button" display="block" gutterBottom>
          {t("roles")}
        </Typography>
        <Select
          closeMenuOnSelect={false}
          className={styles.select}
          defaultValue={values.roles}
          options={roles}
          onChange={onChange}
          isMulti={true}
        />
      </Grid>
    </Grid>
  );
};

export default UserItemFormContainer;
