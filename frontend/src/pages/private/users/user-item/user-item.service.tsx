import { useCallback, useEffect } from "react";
import axios from "axios";

import {
  getUserById,
  postUser,
  putUserById,
  deleteUserById
} from "../../../../services/user.service";
import { getAllRole } from "../../../../services/role.service";

import { User } from "../../../../constants/model";

import useMessage from "../../../../context/message.contex";

const CancelToken = axios.CancelToken;
let cancel: any;

function useUserService() {
  const { pushMsgError } = useMessage();

  const clearRequest = useCallback(function clearRequest() {
    cancel && cancel();
  }, []);

  useEffect(() => {
    return clearRequest;
  }, [clearRequest]);

  const apiGetAllRole = useCallback(
    async function apiGetAllRole() {
      clearRequest();

      return await getAllRole({
        options: {
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiGetUserById = useCallback(
    async function apiGetUserById(id: any) {
      clearRequest();

      return await getUserById({
        id,
        options: {
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiPostUser = useCallback(
    function apiPostUser(data: User) {
      clearRequest();

      return postUser({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiPutUserById = useCallback(
    function apiPutUserById(data: User) {
      clearRequest();

      return putUserById({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiDeletetUserById = useCallback(
    function apiDeletetUserById(data: User) {
      clearRequest();

      return deleteUserById({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  return {
    apiGetAllRole,
    apiGetUserById,
    apiPostUser,
    apiPutUserById,
    apiDeletetUserById,
    clearRequest
  };
}

export { useUserService as default };
