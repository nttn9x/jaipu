import { useEffect, useState, useCallback } from "react";

import { User, Role } from "../../../../constants/model";
import useUserService from "./user-item.service";

import { ROUTES } from "../../../../constants/navigation";
import { BlockUI } from "../../../../constants/common";

import clone from "clone";

const stateDefault = {
  _id: "0",
  username: "",
  password: "",
  avatar: "",
  fullName: "",
  email: "",
  roleIds: []
};

function useUserHook(
  history: any,
  match: any,
  updateItemInList: any,
  removeItemInList: any
) {
  const {
    clearRequest,
    apiGetAllRole,
    apiGetUserById,
    apiPostUser,
    apiPutUserById,
    apiDeletetUserById
  } = useUserService();
  const [isLoading, setLoading] = useState<boolean>(true);
  const [blockUI, setBlockUI] = useState<BlockUI>({
    isBlocking: false,
    message: ""
  });
  const [data, setData] = useState<User>({ ...stateDefault });
  const [roles, setRoles] = useState<Role[]>([]);

  const asyncCall = useCallback(
    async function asyncCall(id: any) {
      clearRequest();

      setLoading(true);

      const resRole = await apiGetAllRole();
      const roles: any = [];
      if (!resRole.isError) {
        resRole.data.forEach(role => {
          roles.push({ value: role._id, label: role.name });
        });
      }

      const resUser = await apiGetUserById(id);

      if (!resUser.isError) {
        const arr: any = [];
        if (resUser.data.roleIds) {
          resUser.data.roleIds.forEach((roleId: any) => {
            const role = roles.find((r: any) => r.value === roleId);
            if (role) {
              arr.push({ value: role.value, label: role.label });
            }
          });
        }
        resUser.data.roles = arr;

        setData(resUser.data);
      }

      setRoles(roles);
      setLoading(false);
    },
    [clearRequest, apiGetUserById, apiGetAllRole]
  );

  useEffect(() => {
    let id = match.params.id;

    if (id !== "0") {
      asyncCall(id);
    } else {
      setLoading(true);

      setTimeout(() => {
        setData({ ...stateDefault });

        setLoading(false);
      }, 300);
    }

    return function clear() {};
  }, [asyncCall, match.params.id, match.params.index]);

  async function updateUser(user: User) {
    setBlockUI({
      isBlocking: true,
      message: "updating"
    });

    const res = await apiPutUserById(user);

    setBlockUI({
      isBlocking: false
    });

    return res;
  }

  async function saveUser(user: User) {
    setBlockUI({
      isBlocking: true,
      message: "saving"
    });

    const res = await apiPostUser(user);
    if (!res.isError) {
      setData(res.data);

      history.push(`${ROUTES.Users}/${user._id}/${match.params.index}`);
    }

    setBlockUI({
      isBlocking: false
    });

    return res;
  }

  async function doSave(data: any) {
    const user = clone(data);
    user.roleIds = [];
    if (user.roles) {
      user.roles.forEach((role: any) => {
        user.roleIds.push(role.value);
      });
      delete user.roles;
    }

    let res;
    if (data._id !== "0") {
      res = await updateUser(user);
    } else {
      res = await saveUser(user);
    }

    if (!res.isError) {
      updateItemInList(user);
    }
  }

  async function doDelete() {
    setBlockUI({
      isBlocking: true,
      message: "deleting"
    });

    await apiDeletetUserById(data);

    removeItemInList();

    history.push(ROUTES.Users);
  }

  return {
    isLoading,
    blockUI,
    roles,
    data,
    doSave,
    doDelete
  };
}

export default useUserHook;
