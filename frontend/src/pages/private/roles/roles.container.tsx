import React from "react";

import { Route } from "react-router-dom";

import {
  Layout,
  ColumnDetail,
  ColumnList
} from "../../../components/ui-own/layout-page/layout.component";
import RoleList from "./role-list/role-list.container";
import RoleItem from "./role-item/role-item.container";

import { ROUTES } from "../../../constants/navigation";

import useRoleHook from "./role-list/role-list.hook";

interface IRoleContainerProps {
  history: any;
}
 
const RoleContainer: React.FC<IRoleContainerProps> = ({ history }) => {
  const { removeItemInList, updateItemInList, ...stateParent } = useRoleHook(
    history
  );

  return (
    <Layout>
      <ColumnList>
        <RoleList {...stateParent} />
      </ColumnList>
      <ColumnDetail>
        <Route
          render={(props: any) => (
            <RoleItem
              {...props}
              removeItemInList={removeItemInList}
              updateItemInList={updateItemInList}
            />
          )}
          path={`${ROUTES.Roles}/:id/:index`}
        />
      </ColumnDetail>
    </Layout>
  );
};

export default RoleContainer;
