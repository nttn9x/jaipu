import { useEffect, useState, useCallback } from "react";

import { Role } from "../../../../constants/model";
import useRoleService from "./role-item.service";
import { toModel } from "../../../../services/role.service";

import { ROUTES } from "../../../../constants/navigation";
import { BlockUI } from "../../../../constants/common";

const stateDefault = {
  _id: "0",
  name: "",
  url: ""
};

function useRoleHook(
  history: any,
  match: any,
  updateItemInList: any,
  removeItemInList: any
) {
  const {
    clearRequest,
    apiGetRoleById,
    apiPostRole,
    apiPutRoleById,
    apiDeletetRoleById
  } = useRoleService();
  const [isLoading, setLoading] = useState<boolean>(true);
  const [blockUI, setBlockUI] = useState<BlockUI>({
    isBlocking: false,
    message: ""
  });
  const [data, setData] = useState<Role>({ ...stateDefault });

  const asyncCall = useCallback(
    async function asyncCall(id: any) {
      clearRequest();

      setLoading(true);
      const res = await apiGetRoleById(id);
      if (!res.isError) {
        setData(draft => {
          return toModel(res.data);
        });
      }

      setLoading(false);
    },
    [clearRequest, apiGetRoleById]
  );

  useEffect(() => {
    let id = match.params.id;
   
    if (id !== "0") {
      asyncCall(id);
    } else {
      setLoading(true);

      setTimeout(() => {
        setData({ ...stateDefault });

        setLoading(false);
      }, 300);
    }

    return function clear() {};
  }, [asyncCall, match.params.id, match.params.index]);

  async function updateRole(role: Role) {
    setBlockUI({
      isBlocking: true,
      message: "updating"
    });

    const res = await apiPutRoleById(role);

    setBlockUI({
      isBlocking: false
    });

    return res;
  }

  async function saveRole(role: Role) {
    setBlockUI({
      isBlocking: true,
      message: "saving"
    });

    const res = await apiPostRole(role);
    if (!res.isError) {
      setData(res.data);

      history.push(`${ROUTES.Roles}/${role._id}/${match.params.index}`);
    }

    setBlockUI({
      isBlocking: false
    });

    return res;
  }

  async function doSave(role: Role) {
    let res;
    if (data._id !== "0") {
      res = await updateRole(role);
    } else {
      res = await saveRole(role);
    }
    if (!res.isError) {
      updateItemInList(role);
    }
  }

  async function doDelete() {
    setBlockUI({
      isBlocking: true,
      message: "deleting"
    });

    await apiDeletetRoleById(data);

    removeItemInList();

    history.push(ROUTES.Roles);
  }

  return {
    isLoading,
    blockUI,
    data,
    doSave,
    doDelete
  };
}

export default useRoleHook;
