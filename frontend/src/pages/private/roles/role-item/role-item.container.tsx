import React from "react";
import styles from "./role-item.module.scss";

import { Formik } from "formik";
import RoleItemSettingsComponent from "../../../../components/ui-own/actions-detail/actions-detail.component";
import RoleItemFormComponent from "./role-item-form.components";
import SkeletonDetailComponent from "../../../../components/ui-own/skeleton-detail/skeleton-detail.component";

import RoleSchema from "./role-item.validation";

import { useTranslation } from "react-i18next";

import useRoleHook from "./role-item.hook";

interface IRoleItemProps {
  history: any;
  match: any;
  handleInputChange: any;
  removeItemInList: any;
  updateItemInList: any;
}

const RoleItemContainer: React.FC<IRoleItemProps> = ({
  history,
  match,
  removeItemInList,
  updateItemInList
}) => {
  const { t } = useTranslation(["common"]);
  const { isLoading, blockUI, data, doSave, doDelete } = useRoleHook(
    history,
    match,
    updateItemInList,
    removeItemInList
  );

  if (isLoading) {
    return <SkeletonDetailComponent />;
  }
  return (
    <Formik
      initialValues={data}
      validationSchema={RoleSchema}
      onSubmit={(values: any, actions) => {
        doSave(values);
      }}
    >
      {({ handleSubmit, ...rest }) => (
        <form onSubmit={handleSubmit}>
          <RoleItemSettingsComponent
            isNew={data._id === "0"}
            useFormik={true}
            blockUI={blockUI}
            doSave={doSave}
            doDelete={doDelete}
          />
          <RoleItemFormComponent t={t} styles={styles} {...rest} />
        </form>
      )}
    </Formik>
  );
};

export default RoleItemContainer;
