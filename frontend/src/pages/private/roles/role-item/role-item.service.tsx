import { useCallback, useEffect } from "react";
import axios from "axios";

import {
  getRoleById,
  postRole,
  putRoleById,
  deleteRoleById
} from "../../../../services/role.service";

import { Role } from "../../../../constants/model";

import useMessage from "../../../../context/message.contex";

const CancelToken = axios.CancelToken;
let cancel: any;

function useRoleService() {
  const { pushMsgError } = useMessage();

  const clearRequest = useCallback(function clearRequest() {
    cancel && cancel();
  }, []);

  useEffect(() => {
    return clearRequest;
  }, [clearRequest]);

  const apiGetRoleById = useCallback(
    async function apiGetRoleById(id: any) {
      clearRequest();

      return await getRoleById({
        id,
        options: {
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiPostRole = useCallback(
    function apiPostRole(data: Role) {
      clearRequest();

      return postRole({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiPutRoleById = useCallback(
    function apiPutRoleById(data: Role) {
      clearRequest();

      return putRoleById({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  const apiDeletetRoleById = useCallback(
    function apiDeletetRoleById(data: Role) {
      clearRequest();

      return deleteRoleById({
        options: {
          data,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError, clearRequest]
  );

  return {
    apiGetRoleById,
    apiPostRole,
    apiPutRoleById,
    apiDeletetRoleById,
    clearRequest
  };
}

export { useRoleService as default };
