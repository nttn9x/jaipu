import { useEffect, useCallback } from "react";
import axios from "axios";

import { getAllRole } from "../../../../services/role.service";

import useMessage from "../../../../context/message.contex";

const CancelToken = axios.CancelToken;
let cancel: any;

function useRoleServices() {
  const { pushMsgError } = useMessage();

  function clearRequest() {
    cancel && cancel();
  }

  useEffect(() => {
    return clearRequest;
  }, []);

  const apiGetAllRole = useCallback(
    function apiGetAllRole(params: any): any {
      clearRequest();

      return getAllRole({
        options: {
          params,
          cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
          })
        },
        pushMsgError
      });
    },
    [pushMsgError]
  );

  return { apiGetAllRole };
}

export { useRoleServices as default };
