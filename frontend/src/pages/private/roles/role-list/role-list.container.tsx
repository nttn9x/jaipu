import React from "react";
import styles from "./role-list.module.scss";

import List from "../../../../components/ui-own/list-rows/list-rows.component";
import Typography from "../../../../components/ui-libraries/typography";

import { State } from "../role-list/role-list.hook";

interface IRoleListProps {
  state: State;
  handleSearch: any;
  handleClick: any;
  doCreateData: any;
}

const RoleList: React.FC<IRoleListProps> = ({
  state,
  handleSearch,
  handleClick,
  doCreateData
}) => {
  function _rowRenderer(index: number) {
    const row = state.datas[index];

    return (
      <div className={styles.list}>
        <div className={styles.list__avatar}>
          <div className={styles.list__avatar_empty} />
        </div>
        <div className={styles.list__info}>
          <Typography variant="subtitle2">{row.name}</Typography>
        </div>
      </div>
    );
  }

  return (
    <List
      idElement="m-role-l"
      rowHeight={102}
      scrollToIndex={state.indexSelected}
      doCreateData={doCreateData}
      onRowClick={handleClick}
      handleSearch={handleSearch}
      isFirstLoad={state.isFirstLoad}
      isLoading={state.isLoading}
      indexSelected={state.indexSelected}
      rowCount={state.datas.length}
      rowRenderer={_rowRenderer}
    />
  );
};

export default RoleList;
