import React from "react";
import styles from "./fields-list.module.scss";

import List from "../../../../components/ui-own/list-rows/list-rows.component";
import Typography from "../../../../components/ui-libraries/typography";
import TextFieldsIcon from "../../../../components/ui-libraries/icons/text-fields";

import useFieldHook from "./fields-list.hook";

const FieldContainer: React.FC = () => {
  const { isLoading, isFirstLoad, datas, handleSearch } = useFieldHook();

  function _rowRenderer(index: number) {
    const row = datas[index];
    return (
      <div className={styles.list}>
        <div className={styles.list__avatar}>
          <TextFieldsIcon />
        </div>
        <div className={styles.list__info}>
          <Typography variant="subtitle2">{row.name}</Typography>
        </div>
      </div>
    );
  }

  return (
    <List
      idElement="m-field-l"
      rowHeight={75}
      handleSearch={handleSearch}
      isFirstLoad={isFirstLoad}
      isLoading={isLoading}
      rowCount={datas.length}
      rowRenderer={_rowRenderer}
    />
  );
};

export default FieldContainer;
