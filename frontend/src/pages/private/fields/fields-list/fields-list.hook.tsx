import { useEffect, useState, useCallback } from "react";

import useFieldServices, { Field } from "../../../../services/field.service";

function useFieldHook() {
  const { getAllField } = useFieldServices();
  const [isFirstLoad, setSetFirstLoad] = useState<boolean>(true);
  const [isLoading, setLoading] = useState<boolean>(false);
  const [datas, setDatas] = useState<Field[]>([]);

  const loadData = useCallback(
    async (textsearch?: string) => {
      const result = await getAllField({ search: textsearch });
      if (!result.isError) {
        setDatas(result.data);
      }
    },
    [getAllField]
  );

  useEffect(() => {
    const runFirstLoad = async () => {
      setSetFirstLoad(true);

      await loadData();

      setSetFirstLoad(false);
    };
    runFirstLoad();
  }, [loadData]);

  async function handleSearch(textsearch: string) {
    setLoading(true);

    await loadData(textsearch);

    setLoading(false);
  }

  return { isFirstLoad, isLoading, datas, handleSearch };
}

export default useFieldHook;
