import React from "react";

import {
  Layout,
  ColumnDetail,
  ColumnList
} from "../../../components/ui-own/layout-page/layout.component";
import FieldList from "./fields-list/fields-list.container";

const FieldContainer: React.FC = () => {
  return (
    <Layout>
      <ColumnList>
        <FieldList /> 
      </ColumnList>
      <ColumnDetail></ColumnDetail>
    </Layout>
  );
};

export default FieldContainer;
 