import React from "react";

import {
  Layout,
  ColumnDetail,
  ColumnList
} from "../../../components/ui-own/layout-page/layout.component";

const ImportContainer: React.FC = () => {
  return (
    <Layout>
      <ColumnList></ColumnList>
      <ColumnDetail></ColumnDetail>
    </Layout>
  );
};

export default ImportContainer;
