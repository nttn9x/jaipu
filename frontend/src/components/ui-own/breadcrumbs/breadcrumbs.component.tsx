import React, { useEffect, useState } from "react";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import styles from "./breadcrumbs.module.scss";

import Breadcrumbs from "../../ui-libraries/breadcrumbs";
import Typography from "../../ui-libraries/typography";
import HomeIcon from "../../ui-libraries/icons/home";

import { ROUTES, SIDE_BAR } from "../../../constants/navigation";

import { useTranslation } from "react-i18next";

const BreadcrumbsComp: React.FC<RouteComponentProps> = props => {
  const [pathName, setPathName] = useState<string>();
  const { t } = useTranslation(["common"]);
  const {
    match: { path }
  } = props;

  useEffect(() => {
    const r = SIDE_BAR.find(r => r.linkTo === path);
    if (r) {
      setPathName(r.keyi18n);
    }
  }, [path]);

  return (
    <Breadcrumbs className={styles.breadcrumbs} aria-label="breadcrumb">
      <Link color="inherit" to={ROUTES.Home}>
        <HomeIcon />
        {t("home")}
      </Link>
      {pathName && (
        <Link color="inherit" to={`/${pathName}`}>
          <Typography color="textPrimary">{t(pathName)}</Typography>
        </Link>
      )}
    </Breadcrumbs>
  );
};

export default withRouter(BreadcrumbsComp);
