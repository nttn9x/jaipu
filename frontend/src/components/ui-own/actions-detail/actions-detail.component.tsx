import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";

import styles from "./actions-detail.module.scss";

import Popover from "../../ui-libraries/popover";
import Button from "../../ui-libraries/button";
import MenuList from "../../ui-libraries/menu-list";
import MenuItem from "../../ui-libraries/menu-item";
import IconButton from "../../ui-libraries/icon-button";
import Divider from "../../ui-libraries/divider";
import Typography from "../../ui-libraries/typography";

import SettingsIcon from "../../ui-libraries/icons/settings";

import { BlockUI } from "../../../constants/common";

import { useTranslation } from "react-i18next";

interface IActionDetailProps {
  isNew: boolean;
  useFormik: boolean;
  doSave: any;
  doDelete: any;
  blockUI: BlockUI;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: theme.palette.secondary.light
  }
}));

const ActionsDetail: React.FC<IActionDetailProps> = ({
  isNew,
  useFormik,
  doSave,
  doDelete,
  blockUI
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const { t } = useTranslation(["common"]);

  function handleClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleDelete() {
    doDelete();

    handleClose();
  }

  const open = Boolean(anchorEl);

  if (blockUI.isBlocking) {
    return (
      <div
        className={`${styles.actions} ${classes.root} ${styles["actions--blocking"]}`}
      >
        <Typography color="textPrimary" variant="button" align="center">
          {t(blockUI.message || "saving")}
        </Typography>
      </div>
    );
  }

  return (
    <div className={styles.actions}>
      {!isNew && (
        <>
          <IconButton aria-label="settings" onClick={handleClick}>
            <SettingsIcon />
          </IconButton>
          <Divider
            className={styles.divider}
            variant="middle"
            orientation="vertical"
          />
        </>
      )}
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
      >
        <MenuList>
          <MenuItem onClick={handleDelete}>{t("delete")}</MenuItem>
        </MenuList>
      </Popover>

      {useFormik ? (
        <Button
          color="primary"
          variant="contained"
          aria-label="save or update"
          type="submit"
        >
          {t(isNew ? "save" : "update")}
        </Button>
      ) : (
        <Button
          color="primary"
          variant="contained"
          aria-label="save or update"
          onClick={doSave}
        >
          {t(isNew ? "save" : "update")}
        </Button>
      )}
    </div>
  );
};

export default ActionsDetail;
