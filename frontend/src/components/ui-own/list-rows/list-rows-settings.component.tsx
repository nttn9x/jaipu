import React, { useState } from "react";

import Typography from "../../ui-libraries/typography";
import Button from "../../ui-libraries/button";
import InputBase from "../../ui-libraries/input-base";
import IconButton from "../../ui-libraries/icon-button";
import Divider from "../../ui-libraries/divider";
import Tooltip from "../../ui-libraries/tooltip";

import SearchIcon from "../../ui-libraries/icons/search";
import FilterListIcon from "../../ui-libraries/icons/filter-list";

import { TIMEOUT } from "../../../constants/common";

interface IListRowSettingProps {
  t: any;
  rowCount: number;
  styles: any;
  handleSearch: any;
  doCreateData: any;
}

let timeoutId: any = 0;

const ListRowSettings: React.FC<IListRowSettingProps> = ({
  t,
  rowCount,
  styles,
  handleSearch,
  doCreateData
}) => {
  const [textsearch, setTextSearch] = useState<string>("");

  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    clearTimeout(timeoutId);

    const value = event.target.value;
    setTextSearch(value);

    timeoutId = setTimeout(() => {
      handleSearch(value);
    }, TIMEOUT.Search);
  }

  return (
    <>
      <div className={styles.list__actions}>
        <div className={styles.list__count}>
          <Typography variant="h6" color="textPrimary">
            {rowCount}
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            {t("total")}
          </Typography>
        </div>
        <div>
          <Button variant="contained" color="primary" onClick={doCreateData}>
            {t("add")}
          </Button>
        </div>
      </div>

      <div className={styles.list__search}>
        <InputBase
          onChange={handleChange}
          className={styles.list__search_input}
          value={textsearch}
          placeholder={t("search")}
          inputProps={{ "aria-label": "Search" }}
        />
        <IconButton aria-label="search">
          <SearchIcon />
        </IconButton>
        <Divider
          className={styles.list__search_divider}
          orientation="vertical"
        />
        <Tooltip title={t("show_search_options")}>
          <IconButton color="secondary" aria-label="Show search options">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      </div>
    </>
  );
};

export default ListRowSettings;
