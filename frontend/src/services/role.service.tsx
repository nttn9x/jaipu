import {
  apiGet,
  Response,
  apiDelete,
  apiPut,
  apiPost
} from "../utils/service.util";

import { API_MODULES } from "../constants/common";
import { Role } from "../constants/model";

import _get from "lodash/get";

export interface Options {
  id?: any;
  options?: any;
  pushMsgError?: any;
}

export function toModel(role: Role) {
  return {
    _id: _get(role, "_id", "0"),
    name: _get(role, "name", ""),
    url: _get(role, "url", "")
  };
}

export function getAllRole({
  options,
  pushMsgError
}: Options): Response<Role[]> {
  return apiGet(
    {
      url: API_MODULES.Roles,
      ...options
    },
    pushMsgError
  );
}

export function getRoleById({
  id,
  options,
  pushMsgError
}: Options): Response<Role> {
  return apiGet(
    {
      url: `${API_MODULES.Roles}/${id}`,
      ...options
    },
    pushMsgError
  );
}

export function postRole({ options, pushMsgError }: Options): Response<Role> {
  return apiPost(
    {
      url: API_MODULES.Roles,
      ...options
    },
    pushMsgError
  );
}

export function putRoleById({
  options,
  pushMsgError
}: Options): Response<Role> {
  return apiPut(
    {
      url: `${API_MODULES.Roles}/${options.data._id}`,
      ...options
    },
    pushMsgError
  );
}

export function deleteRoleById({ options, pushMsgError }: Options) {
  return apiDelete(
    {
      url: `${API_MODULES.Roles}/${options.data._id}`,
      ...options
    },
    pushMsgError
  );
}
