import {
  apiGet,
  Response,
  apiDelete,
  apiPut,
  apiPost
} from "../utils/service.util";

import { API_MODULES } from "../constants/common";
import { User } from "../constants/model";

import _get from "lodash/get";

export interface Options {
  id?: any;
  options?: any;
  pushMsgError?: any;
}

export function toModel(user: User) {
  return {
    _id: _get(user, "_id", "0"),
    username: _get(user, "username", ""),
    password: _get(user, "password", ""),
    fullName: _get(user, "fullName", ""),
    email: _get(user, "email", ""),
    avatar: _get(user, "avatar", "")
  };
}

export function getAllUser({
  options,
  pushMsgError
}: Options): Response<User[]> {
  return apiGet(
    {
      url: API_MODULES.Users,
      ...options
    },
    pushMsgError
  );
}

export function getUserById({
  id,
  options,
  pushMsgError
}: Options): Response<User> {
  return apiGet(
    {
      url: `${API_MODULES.Users}/${id}`,
      ...options
    },
    pushMsgError
  );
}

export function postUser({ options, pushMsgError }: Options): Response<User> {
  return apiPost(
    {
      url: API_MODULES.Users,
      ...options
    },
    pushMsgError
  );
}

export function putUserById({
  options,
  pushMsgError
}: Options): Response<User> {
  return apiPut(
    {
      url: `${API_MODULES.Users}/${options.data._id}`,
      ...options
    },
    pushMsgError
  );
}

export function deleteUserById({ options, pushMsgError }: Options) {
  return apiDelete(
    {
      url: `${API_MODULES.Users}/${options.data._id}`,
      ...options
    },
    pushMsgError
  );
}
