import { lazy } from "react";
import { RouteProps } from "react-router-dom";

import { ROUTES } from "../../constants/navigation";

const Dashboard = lazy(() =>
  import("../../pages/private/dashboard/dashboard.container")
);

const Batches = lazy(() =>
  import("../../pages/private/batches/batches.container")
);

const DatabaseViewer = lazy(() =>
  import("../../pages/private/database-viewer/database-viewer.container")
);

const Export = lazy(() =>
  import("../../pages/private/export/export.container")
);

const Fields = lazy(() =>
  import("../../pages/private/fields/fields.container")
);

const Home = lazy(() => import("../../pages/private/home/home.container"));

const Import = lazy(() =>
  import("../../pages/private/import/import.container")
);

const Monitor = lazy(() =>
  import("../../pages/private/monitor/monitor.container")
);

const Quality = lazy(() =>
  import("../../pages/private/quality/quality.container")
);

const Roles = lazy(() => import("../../pages/private/roles/roles.container"));

const Users = lazy(() => import("../../pages/private/users/users.container"));

const routes: RouteProps[] = [
  {
    exact: true,
    path: ROUTES.Home,
    component: Home
  },
  {
    path: ROUTES.Dashboard,
    component: Dashboard
  },
  {
    path: ROUTES.Batches,
    component: Batches
  },
  {
    path: ROUTES.DatabaseViewer,
    component: DatabaseViewer
  },
  {
    path: ROUTES.Export,
    component: Export
  },
  {
    path: ROUTES.Fields,
    component: Fields
  },
  {
    path: ROUTES.Import,
    component: Import
  },
  {
    path: ROUTES.Monitor,
    component: Monitor
  },
  {
    path: ROUTES.Quality,
    component: Quality
  },
  {
    path: ROUTES.Roles,
    component: Roles
  },
  {
    path: ROUTES.Users,
    component: Users
  }
];

export default routes;
