import React, { Suspense } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import PublicTypeRoute from "./public/public.type.route";
import PrivateTypeRoute from "./private/private.type.route";

import LoadingComponent from "../components/ui-own/progress/loader/loader.component";
import LayoutComponent from "../components/ui-own/layout-app/layout.component";

import AuthProvider from "../context/auth.context";
import ThemeProvider from "../context/theme.context";
import { SnackbarProvider } from "notistack";

const Root: React.FC = () => {
  return (
    <Router>
      <AuthProvider>
        <ThemeProvider>
          <SnackbarProvider>
            <LayoutComponent>
              <Suspense fallback={<LoadingComponent />}>
                <PublicTypeRoute />
                <PrivateTypeRoute />
              </Suspense>
            </LayoutComponent>
          </SnackbarProvider>
        </ThemeProvider>
      </AuthProvider>
    </Router>
  );
};

export default Root;
