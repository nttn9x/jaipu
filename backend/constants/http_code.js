const HTTP_CODE = {
  OK: 200,
  NOT_FOUND: 404,
  UNAUTHORIZED: 401
};

module.exports = HTTP_CODE;
