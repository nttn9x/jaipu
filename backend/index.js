require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const compression = require("compression");
const bodyParser = require("body-parser");
const database = require("./common/database");
const auth = require("./middleware/auth.middleware");
const routes = require("./routes");

const app = express();
const port = process.env.SERVER_PORT;

// Request
app.use(compression()); // Gzip
app.use(express.json())
app.use(bodyParser.json());

// Auth
//app.use(auth);

// Logging
app.use(morgan("tiny"));

// Routes
app.use(routes);

// Middleware for handling Error
// Sends Error Response Back to User
app.use((err, req, res, next) => {
  if (err.status) {
    res.status(err.status).json({
      error: err.message
    });
  } else {
    console.log(err);
    res.status(400).send(err);
  }
});

// Database
database.connectToServer(function (err, client) {
  if (err) console.log(err);

  console.log(`Connected successfully to server ${process.env.DB_URL}`);

  app.listen(port, () => console.log(`Jaipu app listening on port ${port}!`));
});