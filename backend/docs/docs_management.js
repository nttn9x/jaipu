'use strict';
const CrudManagement = require('../dao/crud_management');
const DecoratedParamDao = require('../dao/decorated_param_dao');
//const BadRequestError = require('../errors-handler/bad_request_error');

module.exports = class DocumentManagement extends CrudManagement {
    constructor(pool) {

        let dao = new DecoratedParamDao(pool, 'documents', []);
        super(dao);
        this._dao = dao;
        this._collection_name = 'documents';
    }

    create(items, callback) {
        super.create(items, callback);
    }

    update_by_id(id, datas, callback) {
        super.update_by_id(id, datas, callback);
    }

    delete_by_id(id, callback) {

        super.delete_by_id(id, callback);
    }

    get_item_by_id(id, callback) {

        super.get_item_by_id(id, callback);
    }

    get_all(limit, callback) {

        super.get_all(limit, callback);
    }

    find_acquisition_info_exist_in_datastore(list_document_link_hash, callback) {
        if (!Array.isArray(list_document_link_hash) || list_document_link_hash.length === 0) {
            return callback(null, []);
        }


        let selector = {
            $or: []
        };
        for (let document_link_hash of list_document_link_hash) {
            selector.$or.push({
                'document_link.hash': document_link_hash
            });
        }
        let params = {
            selector: selector
        };
        this._dao.read(params, callback);
    }

    get_item_by_collect_session_id(collect_session_id, callback) {

        let params = {
            selector: {
                'collect_session_id': collect_session_id
            }
        };
        this._dao.read(params, callback);
    }

    delete_by_list_id(list_id, callback) {

        let params = {
            selector: {
                '_id': list_id
            }
        };
        this._dao.delete(params, callback);
    }
}