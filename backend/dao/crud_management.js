'use strict';
const NotFoundError = require('../errors-handler/not_found_error');

module.exports = class BaseManagement {
    constructor(dao) {
        this._dao = dao;
    }

    create(items, callback) {
        this._dao.create(items, callback);
    };

    update_by_id(id, datas, callback) {
        let params = {
            selector: { '_id': id },
            datas: datas
        };
        this._dao.update(params, (err, result) => {
            if (err) return callback(err);
            if ((result.n || 0) === 0) {
                return callback(new NotFoundError(`Data with ID (${id || 'undefined'}) not found.`));
            }
            this._dao.read({ selector: { '_id': id } }, (err, result) => {
                if (err) return callback(err);
                callback(null, result[0]);
            });
        });
    }

    update_by_conditions(conditions, datas, callback) {
        let params = {
            selector: conditions,
            datas: datas,
            options: {
                multi: true
            }
        };
        this._dao.update(params, (err, result) => {
            if (err) return callback(err);
            if ((result.n || 0) === 0) {
                return callback(new NotFoundError(`Data with conditions (${JSON.stringify(conditions) || 'undefined'}) not found.`));
            }
            this._dao.read({selector:params.selector}, (err, result) => {
                if (err) return callback(err);
                callback(null, result[0]);
            });
        });
    }

    delete_by_id(id, callback) {
        let params = {
            selector: { '_id': id }
        };
        this._dao.delete(params, callback);
    }

    delete_by_list_id(list_id, callback) {
        if (!Array.isArray(list_id) || list_id.length === 0) {
            return callback(null, []);
        }
        let arr = [];
        for (let id of list_id) {
            arr.push(id);
        }
        let selector = { '_id': { $in: arr } };
        let params = {
            selector: selector
        };
        this._dao.delete(params, callback);
    }

    get_all(limit, callback) {
        var params = {
            options: {
                limit: limit
            }
        };
        this._dao.read(params, callback);
    }

    get_item_by_id(id, callback) {
        let params = {
            selector: { '_id': id },
        };
        this._dao.read(params, (err, result) => {
            if (err) return callback(err);
            callback(null, result[0]);
        });
    }

    get_item_by_conditions(conditions, callback) {
        this._dao.read(conditions, (err, result) => {
            if (err) return callback(err);
            callback(null, result);
        });
    }
    
    delete_by_parent_id(id, parent_field, callback) {
        let params = {
            selector: {}
        };
        params['selector'][parent_field] = id;
        this._dao.delete(params, callback);
    }

    /**
     * 
     * @param {Array} list_id 
     * @param {*} datas 
     * @param {Function} callback
     */
    update_by_list_id(list_id, datas, callback) {
        let arr = [];
        for (let id of list_id) {
            arr.push(id);
        }
        let params = {
            selector: { '_id': { '$in': list_id } },
            datas: datas,
            options: {
                multi: true
            }
        };
        this._dao.update(params, (err, result) => {
            if (err) return callback(err);
            if ((result.n || 0) === 0) {
                return callback(new NotFoundError(`Data with IDs (${arr || 'undefined'}) not found.`));
            }
            this._dao.read({ selector: { '_id': { $in: arr } } }, (err, result) => {

                if (err) return callback(err);
                callback(null, result);
            });
        });
    }

    /**
     * @desc Calculates aggregate values for the data
     * @param {*} params 
     * @param {Function} callback
     */
    aggregate_items_from_multi_collections(params, callback) {       
        let param = {
            selector: params
        };
        this._dao.aggregate(param, (err, result) => {
            if (err) return callback(err);
            callback(null, result);
        });
    }

    /**
     * @desc Finds the distinct values for a specified field across a single collection or view and returns the results in an array.
     * @param {String} fields 
     * @param {JSON} selector 
     * @param {*} options 
     * @param {Function} callback
     */
    distinct(fields, selector, options, callback) {        
        let params = {
            fields: fields,
            selector: query || '',
            options: options || ''
        };  
        this._dao.distinct(params, (err, result) => {
            if (err) return callback(err);
            callback(null, result);
        });
    }

    /**
     * @desc Returns the count of documents
     * @param {JSON} query 
     * @param {*} options 
     * @param {Function} callback
     */
    count(query, options, callback) {
        let params = {
            selector: query,
            options: options || ''
        };  
        this._dao.count(params, (err, result) => {
            if (err) return callback(err);
            callback(null, result);
        });
    }
}