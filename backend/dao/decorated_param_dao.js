'use strict';
const BadRequestError = require('../errors-handler/bad_request_error');
const ObjectId = require('mongodb').ObjectId;
const Dao = require('./dao');

module.exports = class DecoratedParamDao extends Dao {
    constructor(pool, collection_name, unique_fields) {
        super(pool, collection_name, unique_fields);
    }

    _check_param(params, action, callback) {
        if (!params.selector) {
            params.selector = {};
        }

        if (params.selector._id) {
            try {
                let list_id = [];
                if (Array.isArray(params.selector._id)) {
                    params.selector._id.forEach(function (id) {
                        list_id.push(new ObjectId(id));
                    });
                    params.selector._id = {
                        $in: list_id
                    }
                } else {
                    if (params.selector._id.$in && Array.isArray(params.selector._id.$in)) {
                        params.selector._id.$in.forEach(function (id) {
                            list_id.push(new ObjectId(id));
                        });
                        params.selector._id = {
                            $in: list_id
                        }
                    } else if (params.selector._id.$or && Array.isArray(params.selector._id.$or)) {
                        params.selector._id.$or.forEach(function (id) {
                            list_id.push(new ObjectId(id));
                        });
                        params.selector._id = {
                            $or: list_id
                        }
                    } else {
                        params.selector._id = new ObjectId(params.selector._id);
                    }
                }
            } catch (error) {
                return callback(new BadRequestError("Cannot cast Id to ObjectId"));
            }
        }


        if (!params.fields && action === 'read') {
            params.fields = {};
        }

        if (!params.options) {
            params.options = {};
        }

        if (action === 'update') {
            if (params.datas) {
                params.datas.$set ? delete params.datas.$set.id : delete params.datas.id;
                params.datas.$set ? params.datas.$set.last_modified = new Date() : params.datas.last_modified = new Date();
                if (params.datas.$set) {
                    delete params.datas.$set.created_date;
                    return callback(null, params);
                } else if (params.datas.$push || params.datas.$addToSet ||
                    params.datas.$pop || params.datas.$pull || params.datas.$pullAll || params.datas.$each ||
                    params.datas.$position || params.datas.$slice || params.datas.$sort) {
                    params.datas.$set = {};
                    params.datas.$set.last_modified = params.datas.last_modified || new Date();
                    delete params.datas.last_modified;
                    return callback(null, params);
                } else {
                    super.read(params.selector._id, (err, result) => {
                        if (err) return callback(err);
                        if (result[0].created_date) {
                            params.datas.created_date = new Date(result[0].created_date);
                            return callback(null, params);
                        } else {
                            return callback(null, params);
                        }
                    });
                }
            } else {
                params.datas = {};
                return callback(null, params);
            }
        } else {
            return callback(null, params);
        }
    }

    create(items, callback) {
        if (typeof (items.length) === 'undefined') {
            items = [items];
        }
        for (let i = 0, ii = items.length; i < ii; i++) {
            if (items[i]._id) {
                return callback(new BadRequestError("Attempting to create doc: " + items[i] + "which is already persisted"));
            }
            delete items[i].id;
            items[i].created_date = new Date();
            items[i].last_modified = new Date();
        }
        super.create(items, callback);
    }

    read(params, callback) {
        this._check_param(params, 'read', (err, params) => {
            if (err) return callback(err);
            super.read(params, callback);
        });
    }

    update(params, callback) {
        this._check_param(params, 'update', (err, params) => {
            if (err) return callback(err);
            super.update(params, callback);
        });
    }

    delete(params, callback) {
        this._check_param(params, 'delete', (err, params) => {
            if (err) return callback(err);
            super.delete(params, callback);
        });
    }

    decorated_object_id(id) {
        if (id) {
            try {
                id = new ObjectId(id);
            } catch (error) {
                throw new BadRequestError("Cannot cast Id to ObjectId");
            }

        } else {
            throw new BadRequestError("Id is not exists");
        }
        return id;
    }
}