'use strict';
const BadRequestError = require('../errors-handler/bad_request_error');
const ConflictError = require('../errors-handler/conflict_error');
const NotFoundError = require('../errors-handler/not_found_error');
const SystemError = require('../errors-handler/system_error');
/**
 * Generic DAO.
 * 
 * This DAO implementation fulfills C and R of CRUD.
 * It can create and pull from any collection given a basic 
 * params object.
 *
 * This dao uses a generic-pool connection pool by default.
 *
 * @param pool The generic-pool connection pool.
 * @param collection The name of the collection.
 */
module.exports = class Dao {
    constructor(pool, collection_name, unique_fields) {
        this.pool = pool;
        this.collection_name = collection_name;
        this.unique_fields = unique_fields;
    }
    /**
     * Gets the collection by name using the current connection string and collection name.
     */
    get_collection(callback) {
        callback(null, this.pool.getCollection(this.collection_name));
        //this.get_collection_by_name(this.collection_name, this.unique_fields, callback);
    }
    /**
     * Gets a collection by name
     */
    get_collection_by_name(collection_name, fields, callback) {
        this.pool.collection(collection_name, (err, collection) => {
            if (err) return callback(new SystemError(err));
            //self.pool.release(client);
            let unique_name = '';
            let unique_fields = [];
            if (!Array.isArray(fields)) {
                return callback(new SystemError('unique_fields is array type'));
            }
            if (fields.length === 0) {
                return callback(err, collection);
            }

            for (let i = 0, ii = fields.length; i < ii; i++) {
                unique_name += '_' + fields[i];
                unique_fields.push([fields[i], 1]);
            }
            unique_name = 'index' + unique_name;
            if (unique_name.length > 128) {
                unique_name = unique_name.substring(0, 127);
            }
            let unique_options = {
                unique: true,
                background: true,
                dropDups: true,
                name: unique_name
            };
            collection.createIndex(unique_fields, unique_options, (err, indexName) => {
                if (err) {
                    callback(new SystemError(err));
                    return;
                }
                callback(err, collection);
            });
        });
    }
    /**
     * Persists a number of docs or single doc.
     */
    create(items, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            if (items.length > 0) {
                collection.insertMany(items, (err, results) => {
                    if (err) {
                        if (err.code === 11000) {
                            return callback(new ConflictError(err))
                        } else {
                            return callback(new SystemError(err));
                        }
                    }
                    callback(null, results);
                });
            } else {
                callback(new NotFoundError("Empty items"));
            }
        });
    }
    /**
     * Reads a list of items from the datastore.
     */
    read(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.find(params.selector, params.fields, params.options, (err, result) => {
                if (err) return callback(new BadRequestError(err));
                result.toArray(function (error, results) {
                    if (error) return callback(new SystemError(error));
                    callback(null, results);
                });
            });
        });
    }

    /**
     * @desc Updates a list of items from the datastore.
     * @param {JSON} params 
     * @param {Function} callback 
     */
    update(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.update(params.selector, params.datas, params.options, (err, result) => {
                if (err) {
                    if (err.code === 11000) {
                        return callback(new ConflictError(err))
                    } else {
                        return callback(new BadRequestError(err));
                    }
                }
                callback(null, result.result);
            });
        });
    }


    /**
     * delete a list of items from the datastore.
     */
    delete(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.deleteMany(params.selector, params.options, (err, result) => {
                if (err) return callback(new BadRequestError(err));
                callback(null, result.result || result);
            });
        });
    }

    /**
     * @desc Calculates aggregate values for the data
     * @param {JSON} params 
     * @param {Function} callback 
     */
    aggregate(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.aggregate(params.selector || params, params.config || {}, (err, result) => {
                if (err) return callback(new BadRequestError(err));
                callback(null, result);
            });
        });
    }

    /**
     * @desc Finds the distinct values for a specified field across a single collection or view and returns the results in an array.
     * @param {JSON} params 
     * @param {Function} callback 
     */
    distinct(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.distinct(params.fields, params.selector, params.options, (err, result) => {
                if (err) return callback(new BadRequestError(err));
                callback(null, result);
            });
        });
    }

    /**
     * @desc Returns the count of documents
     * @param {JSON} params 
     * @param {Function} callback 
     */
    count(params, callback) {
        this.get_collection((err, collection) => {
            if (err) return callback(new SystemError(err));
            collection.count(params.selector, params.options, (err, result) => {
                if (err) return callback(new BadRequestError(err));
                callback(null, result);
            });
        });
    }
}