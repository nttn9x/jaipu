const HTTP_CODE = require("../constants/http_code");

function ok(res, json) {
  if (json) {
    return res
      .status(HTTP_CODE.OK)
      .json(json)
      .end();
  }
  return res.status(HTTP_CODE.OK).end();
}

function error() {
  return { status: HTTP_CODE.NOT_FOUND, message: "Not Found" };
}

function errorNotFound() {
  return { status: HTTP_CODE.NOT_FOUND, message: "Not Found" };
}

module.exports = { ok, error, errorNotFound };
