const fs = require("fs");
const jwt = require("jsonwebtoken");

function auth(req, res, next) {
  try {
    if (req.url !== "/auth/login") {
      const token = req.headers.authorization.split(" ")[1];
      const publicKEY = fs.readFileSync("./secret/public.key", "utf8");
      const verifyOptions = {
        issuer: process.env.JWT_ISSUER,
        subject: process.env.JWT_SUBJECT,
        audience: process.env.JWT_AUDIENCE,
        // expiresIn: process.env.JWT_EXPIRES_IN,
        algorithm: process.env.JWT_ALGORITHM
      };

      jwt.verify(token, publicKEY, verifyOptions);
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({ error: "Not Authorized" });
  }
}

module.exports = auth;
