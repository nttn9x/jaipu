'use strict'
var path = require('path');
var PROJECT_ROOT = path.join(__dirname, '..');

let config;
try {
  config = require('../logger.json');
} catch (error) {
  console.log(error.message);
  config = {
    "level": "debug",
    "console": true,
    "path": "",
    "file_name_default_all": "da-all.log",
    "file_name_default_debug": "da-debug.log",
    "file_name_default_info": "da-info.log",
    "file_name_default_error": "da-error.log",
    "max_size": 5242880,
    "max_files": 3
  }
}

let config_log = config;

let winston = require('winston');
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: {
    service: 'user-service'
  },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
      filename: 'error.log',
      level: 'error'
    }),
    new winston.transports.File({
      filename: 'combined.log'
    })
  ]
});

module.exports = logger;