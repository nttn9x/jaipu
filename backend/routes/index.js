const express = require("express");
const router = express.Router();

const auth = require("./auth/auth.router");
const users = require("./users/users.router");
const roles = require("./roles/roles.router");
const importer = require("./import/importer");
// const fields = require("./fields");

router.use("/auth", auth);

router.use("/users", users);

router.use("/roles", roles);

router.use("/import", importer);

// router.get("/fields", fields);

module.exports = router;