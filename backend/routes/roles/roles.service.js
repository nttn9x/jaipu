const db = require("../../common/database");

async function getAllRole(search) {
  const params = {
    isDelete: { $ne: true }
  };
  if (search) {
    params["$or"] = [
      {
        name: new RegExp(search)
      },
      {
        url: new RegExp(search)
      }
    ];
  }

  return await db
    .getCollection("roles")
    .find(params)
    .toArray();
}

async function postRole(role) {
  if (role.users) {
    role.users.forEach(user => {
      user.user_id = db.getPrimaryKey(user.user_id);
    });
  }

  return await db.getCollection("roles").insertOne(role);
}

async function getRoleById(id) {
  return await db.getCollection("roles").findOne({ _id: db.getPrimaryKey(id) });
}

async function findRoleByName(name) {
  return await db.getCollection("roles").findOne({ name });
}

async function deleteRoleById(id) {
  return await db
    .getCollection("roles")
    .deleteOne({ _id: db.getPrimaryKey(id) });
}

async function updateRoleById(id, body) {
  delete body["_id"];

  return await db
    .getCollection("roles")
    .updateOne({ _id: db.getPrimaryKey(id) }, { $set: body });
}

module.exports = {
  postRole,
  getAllRole,
  getRoleById,
  deleteRoleById,
  updateRoleById,
  findRoleByName
};
