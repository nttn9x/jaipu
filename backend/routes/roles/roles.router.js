const express = require("express");
const {
  postRole,
  getAllRole,
  getRoleById,
  deleteRoleById,
  updateRoleById
} = require("./roles.controller");

const rules = require("./roles.validation");
const router = express.Router();

router.get("/", getAllRole);

router.post("/", rules, postRole);

router.get("/:id", getRoleById);

router.delete("/:id", deleteRoleById);

router.put("/:id", rules, updateRoleById);

module.exports = router;
