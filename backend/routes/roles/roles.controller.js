const service = require("./roles.service");
const { validationResult } = require("express-validator");

async function getAllRole(req, res, next) {
  try {
    const roles = await service.getAllRole(req.query.search);

    res.json(roles);
  } catch (error) {
    next(error);
  }
}

async function postRole(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const role = await service.findRoleByType(req.body.type);
    if (role) {
      return res.status(400).send({ error: "Type has been used already" });
    }
    const data = await service.postRole(req.body);

    res.json(data.ops[0]);
  } catch (error) {
    next(error);
  }
}

async function getRoleById(req, res, next) {
  try {
    const role = await service.getRoleById(req.params.id);

    res.json(role);
  } catch (error) {
    next(error);
  }
}

async function deleteRoleById(req, res, next) {
  try {
    const role = await service.deleteRoleById(req.params.id);

    res.json(role);
  } catch (error) {
    next(error);
  }
}

async function updateRoleById(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const roleExist = await service.findRoleByName(req.body.name);
    if (roleExist &&roleExist._id.toString() !== req.params.id) {
      return res.status(400).send({ error: "Name has been used already" });
    }
    await service.updateRoleById(req.params.id, req.body);

    res.end();
  } catch (error) {
    next(error);
  }
}

module.exports = {
  postRole,
  getAllRole,
  getRoleById,
  deleteRoleById,
  updateRoleById
};
