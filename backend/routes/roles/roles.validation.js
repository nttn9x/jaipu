const { check } = require("express-validator");

const rules = [
  check("name")
    .isLength({ min: 3 })
    .withMessage("Name must be required")
];

module.exports = rules;
