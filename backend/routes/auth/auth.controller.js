const fs = require("fs");
const { validationResult } = require("express-validator");
const { findUserByUsernamePassword } = require("../users/users.service");
const jwt = require("jsonwebtoken");

async function doLogin(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(401).end();
    }
    const { username, password } = req.body;

    const verifyOptions = {
      issuer: process.env.JWT_ISSUER,
      subject: process.env.JWT_SUBJECT,
      audience: process.env.JWT_AUDIENCE,
      // expiresIn: process.env.JWT_EXPIRES_IN,
      algorithm: process.env.JWT_ALGORITHM
    };
    const privateKEY = fs.readFileSync("./secret/private.key", "utf8");

    const user = await findUserByUsernamePassword(username, password);
    if (!user) {
      return res.status(401).end();
    }

    delete user.password;

    const token = jwt.sign(user, privateKEY, verifyOptions);

    res.send({
      token,
      user
    });
  } catch (error) {
    next(error);
  }
}

module.exports = {
  doLogin
};
