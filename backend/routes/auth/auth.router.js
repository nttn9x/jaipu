const express = require("express");
const { doLogin } = require("./auth.controller");
const rules = require("./auth.validation");
const router = express.Router();

router.post("/login", rules, doLogin);

module.exports = router;
