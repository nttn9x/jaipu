const { check } = require("express-validator");

const rules = [
  check("username")
    .isLength({ min: 1 })
    .withMessage("Username must be required"),
  check("password")
    .isLength({ min: 1 })
    .withMessage("Password must be required")
];

module.exports = rules;
