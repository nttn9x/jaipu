'use strict';
var express = require("express");
var multer = require('multer');
var amqp = require('amqplib/callback_api');
var AdmZip = require('adm-zip');
const path = require('path');
const fs = require('fs');

const db = require("../../common/database");
const router = express.Router();

let path_store_zip = process.env.PATH_STORE_ZIP_UPLOAD;
let path_store_images = process.env.PATH_STORE_UNZIP;
let queue_url = `${process.env.QUEUE_URL}`;
let queue_import = `${process.env.QUEUE_NAME_IMPORT}`;
let docs_queue = `${process.env.QUEUE_NAME_DOC}`;

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, path_store_zip);
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
});

var upload = multer({
    storage: storage
}).single('batch');


const DOCManagement = require('../../docs/docs_management');
let doc_management = new DOCManagement(db);
const logger = require('../../logger/logger');

router.post('/upload', (req, res, next) => {
    upload(req, res, function (err, msg) {
        if (err) {
            return res.end("Error uploading file.");
        }
        StepsAfterUploadFile(req.file.filename);
        res.end("File is uploaded");
    });
});

function StepsAfterUploadFile(filename) {
    var file_uploaded = path_store_zip + '/' + filename;
    var record_imported = {
        created_time: new Date(),
        file_imported: file_uploaded,
        unzip_path: '',
        status: 0,
        total_images: 0
    };

    db.getCollection("import").insertOne(record_imported, (errs, rs_import) => {
        if (errs) {
            console.log('Cannot insert data to DB Import collection. ' + err);
            return;
        }
        //console.log(rs_import.ops[0]._id);
        let messagePush = {
            ImportId: rs_import.ops[0]._id,
            PathFile: file_uploaded
        };
        SendMessageToImportQueue(messagePush, (err, status_send) => {
            if (err) {
                return err;
            }
            CosumeMessageFromImportQueue((err, msg) => {
                if (err) return res.end(err.message);
                var ms = JSON.parse(msg);
                _Extract_Zip_File(ms.PathFile, ms.ImportId, filename, (err_extract, items) => {
                    if (err_extract) {
                        logger.error('%j', err_extract);
                        return;
                    }
                    _send_docs_to_queue(items, (err_push, result_push) => {
                        if (err_push) {
                            logger.error('%j', err_push);
                        } else {
                            logger.error('%j', result_push);
                        }
                    });
                });
            });
        });
    });


}

function SendMessageToImportQueue(contentMessage, callback) {
    amqp.connect(queue_url, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            channel.assertQueue(queue_import, {
                durable: false
            });

            channel.sendToQueue(queue_import, Buffer.from(JSON.stringify(contentMessage)));
            //console.log(" [x] Sent %s", contentMessage);
            callback(null, 'success');
        });
    });
};

function CosumeMessageFromImportQueue(callback) {
    amqp.connect(queue_url, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }

            channel.consume(queue_import, function (msg) {
                callback(null, msg.content.toString())
            }, {
                noAck: true
            });
        });
    });
}

function _Extract_Zip_File(pathZip, importId, zipname, callback) {
    var zip = new AdmZip(pathZip);
    zip.extractAllToAsync(path_store_images, true, (err, data_return) => {
        if (err) return;

        var dir_extracted = path_store_images + '/' + zipname.replace('.zip', '');

        fs.readdir(dir_extracted, function (err, files) {
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            }
            let _docs = [];
            files.forEach((file) => {
                let doc_item = {
                    doc_path: dir_extracted + '/' + file.trim(),
                    doc_name: file.trim()
                };
                _docs.push(doc_item);
            });
            _docs = _set_attributes_import_to_docs(importId, _docs);
            doc_management.create(_docs, (err_insert, result_insert) => {
                if (err_insert) {
                    logger.error('%j', err_insert);
                    callback(err_insert);
                }
                callback(null, result_insert.ops);
            });
        });
    });
}

function _send_docs_to_queue(items, callback) {
    amqp.connect(queue_url, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            channel.assertQueue(docs_queue, {
                durable: false
            });

            items.forEach((doc) => {
                let contentMessage = JSON.stringify(doc);
                channel.sendToQueue(docs_queue, Buffer.from(contentMessage));
            });

            //console.log(" [x] Sent %s", contentMessage);
            callback(null, 'success');
        });
    });
};

function _set_attributes_import_to_docs(import_id, list_items) {
    list_items.forEach((doc) => {
        doc.import_id = import_id;
        doc.status = 0;
    });
    return list_items;
}

module.exports = router;

//https://codeforgeek.com/file-uploads-using-node-js/
//https://dzone.com/articles/upload-files-or-images-to-server-using-nodejs