const service = require("./users.service");
const { validationResult } = require("express-validator");

async function getAllUser(req, res, next) {
  try {
    const users = await service.getAllUser(req.query.search);

    res.json(users);
  } catch (error) {
    next(error);
  }
}

async function postUser(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const user = await service.findUserByUsername(req.body.username);
    if (user) {
      return res.status(400).send({ error: "Username has been used already" });
    }
    const data = await service.postUser(req.body);

    res.json(data.ops[0]);
  } catch (error) {
    next(error);
  }
}

async function getUserById(req, res, next) {
  try {
    const user = await service.getUserById(req.params.id);

    res.json(user);
  } catch (error) {
    next(error);
  }
}

async function deleteUserById(req, res, next) {
  try {
    const user = await service.deleteUserById(req.params.id);

    res.json(user);
  } catch (error) {
    next(error);
  }
}

async function updateUserById(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const userExist = await service.findUserByUsername(req.body.username);

    if (userExist && userExist._id.toString() !== req.params.id) {
      return res.status(400).send({ error: "Username has been used already" });
    }
    await service.updateUserById(req.params.id, req.body);

    res.end();
  } catch (error) {
    next(error);
  }
}

module.exports = {
  postUser,
  getAllUser,
  getUserById,
  deleteUserById,
  updateUserById
};
