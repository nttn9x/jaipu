const { check } = require("express-validator");

const rules = [
  check("username")
    .isLength({ min: 3 })
    .withMessage("Username must be at least 3 chars long"),
  check("password")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 chars long")
];

module.exports = rules;
