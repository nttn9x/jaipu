const db = require("../../common/database");

async function getAllUser(search) {
  const params = {
    isRoot: { $ne: true }, // except admin
    isDelete: { $ne: true }
  };
  if (search) {
    params["$or"] = [
      {
        username: new RegExp(search)
      },
      {
        email: new RegExp(search)
      }
    ];
  }

  return await db
    .getCollection("users")
    .find(params)
    .toArray();
}

async function postUser(user) {
  return await db.getCollection("users").insertOne(user);
}

async function getUserById(id) {
  return await db.getCollection("users").findOne({ _id: db.getPrimaryKey(id) });
}

async function findUserByUsername(username) {
  return await db.getCollection("users").findOne({ username });
}

async function deleteUserById(id) {
  return await db
    .getCollection("users")
    .updateOne({ _id: db.getPrimaryKey(id) }, { $set: { isDelete: true } });
}

async function updateUserById(id, body) {
  delete body["_id"];

  return await db
    .getCollection("users")
    .updateOne({ _id: db.getPrimaryKey(id) }, { $set: body });
}

async function findUserByUsernamePassword(username, password) {
  return await db.getCollection("users").findOne({ username, password });
}

module.exports = {
  postUser,
  getAllUser,
  getUserById,
  deleteUserById,
  updateUserById,
  findUserByUsername,
  findUserByUsernamePassword
};
