const express = require("express");
const {
  postUser,
  getAllUser,
  getUserById,
  deleteUserById,
  updateUserById
} = require("./users.controller");

const rules = require("./users.validation");
const router = express.Router();

router.get("/", getAllUser);

router.post("/", rules, postUser);

router.get("/:id", getUserById);

router.delete("/:id", deleteUserById);

router.put("/:id", rules, updateUserById);

module.exports = router;
