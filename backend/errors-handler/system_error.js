module.exports = class SystemError extends Error{
      constructor(message){
            super(message);
            this.code = 500;
      }
}