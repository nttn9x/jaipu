const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectID;
const url = `${process.env.DB_FULL_PATH}`
//`mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL}`;

let _db;

module.exports = {
  connectToServer: function (callback) {
    MongoClient.connect(
      url, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        poolSize: 50
      },
      function (err, client) {
        if (err) {
          console.log(err);
        }
        _db = client.db(process.env.DB_NAME);

        return callback(err);
      }
    );
  },

  getDb: function () {
    return _db;
  },

  getPrimaryKey: function (_id) {
    return ObjectID(_id);
  },

  getCollection: function (name) {
    return _db.collection(name);
  }
};